///A Simple Web Server (WebServer.java)

package HTTP;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;

/**
 * Example program from Chapter 1 Programming Spiders, Bots and Aggregators in
 * Java Copyright 2001 by Jeff Heaton
 *
 * WebServer is a very simple web-server. Any request is responded with a very
 * simple web-page.
 *
 * @author Jeff Heaton
 * @version 1.0
 */
public class WebServer {
    
    /**
     * WebServer constructor.
     */
    protected void start() {
        ServerSocket s;
        
        System.out.println("Webserver starting up on port 80");
        System.out.println("(press ctrl-c to exit)");
        try {
            // create the main server socket
            s = new ServerSocket(3000);
        } catch (Exception e) {
            System.out.println("Error: " + e);
            return;
        }
        
        System.out.println("Waiting for connection");
        for (;;) {
            try {
                // wait for a connection
                Socket remote = s.accept();
                // remote is now the connected socket
                System.out.println("Connection, sending data.");
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        remote.getInputStream()));
                PrintWriter out = new PrintWriter(remote.getOutputStream());
                
                // read the data sent. We basically ignore it,
                // stop reading once a blank line is hit. This
                // blank line signals the end of the client HTTP
                // headers.
                String str = ".";
                while(str != null && !str.equals(""))
                {
                    System.out.println("Waiting for request...");
                    str = "";
                    str = in.readLine();
                    try
                    {
                        System.out.println("Request : [" + str + "]");
                        String[] request = str.split(" ");
                        
                        
                        if(request[0].equals("GET"))
                        {
                            System.out.println("GET received");
                            request[1] = request[1].substring(1);
                            System.out.println("File : [" + request[1] + "]");
                            File correspondingFile = new File(request[1]);
                            if(!correspondingFile.exists()){
                                correspondingFile = new File("HTMLDocs/404.html");
                                out.println("HTTP/1.0 404 File not found");
                            }
                            else{                                
                                out.println("HTTP/1.0 200 OK");
                            }
                            FileInputStream reader = new FileInputStream(correspondingFile);
                            
                            //skip headers
                            while(!str.equals(""))
                                str = in.readLine();
                            
                            
                            // Send the response
                            // Send the headers
                            //out.println("Content-Type: text/html");
                            out.println("Server: Bot");
                            out.println("Content-Length: " + correspondingFile.length());
                            out.println("Accept-Encoding: *");
                            
                            // this blank line signals the end of the headers
                            out.println("");
                            
                            while(reader.available() > 0)
                            {
								out.write(reader.read());
								out.flush();
							}
                                
                            reader.close();      
                        }
                        
                        else if(request[0].equals("POST"))
                        {
                            System.out.println("POST received");
                            
                            while(!str.split(" ")[0].equals("Content-Length:"))
                            {
                                str = in.readLine();
                                System.out.println("Read : [" + str + "]");
                            }
                            
                            int toReadLength = Integer.parseInt(str.split(" ")[1]);
                            
                            System.out.println("parsed length : " + toReadLength);
                            
                            //skipping blank line
                            str = in.readLine();
                            
                            int readContentLength = 0;
                            while(readContentLength < toReadLength)
                            {
                                str = in.readLine();
                                readContentLength += str.length() + 1;
                                
                                System.out.println("Received (" + readContentLength + "/" + toReadLength + ") : [" + str + "]");
                            }
                        }
                        
                        else if(request[0].equals("PUT"))
                        {
                            System.out.println("PUT received");
                            request[1] = request[1].substring(1);
                            System.out.println("File : [" + request[1] + "]");
                            File correspondingFile = new File(request[1]);
                            
                            if(correspondingFile.createNewFile())
                            {
                                System.out.println("Created file - " + request[1]);
                            }
                            
                            FileWriter writer = new FileWriter(correspondingFile);
                            
                            while(!str.split(" ")[0].equals("Content-Length:"))
                            {
                                str = in.readLine();
                            }
                            
                            int toReadLength = Integer.parseInt(str.split(" ")[1]);
                            
                            while(!str.equals(""))
                                str = in.readLine();
                            
                            int readContentLength = 0;
                            while(readContentLength < toReadLength)
                            {
                                str = in.readLine();
                                readContentLength += str.length() + 1;
                                
                                System.out.println("Writing (" + readContentLength + "/" + toReadLength + ") : [" + str + "]");
                                
                                writer.write(str + '\n');
                            }
                            
                            System.out.println("Writing complete !");
                            
                            out.println("HTTP/1.0 201 Created file");
                            out.println("Server: Bot");
                            
                            writer.close();
                        }
                        
                        else if(request[0].equals("DELETE"))
                        {
                            System.out.println("DELETE received");
                            request[1] = request[1].substring(1);
                            System.out.println("File : [" + request[1] + "]");
                            
                            while(!str.equals(""))
                                str = in.readLine();
                            
                            File correspondingFile = new File(request[1]);
                            correspondingFile.delete();
                        }
                        
                        else if(request[0].equals("HEAD"))
                        {
                            System.out.println("HEAD received");
                            request[1] = request[1].substring(1);
                            System.out.println("File : [" + request[1] + "]");
                            
                            while(!str.equals(""))
                                str = in.readLine();
                            
                            File correspondingFile = new File(request[1]);
                            if(!correspondingFile.exists()){
                                correspondingFile = new File("HTMLDocs/404.html");
                                out.println("HTTP/1.0 404 File not found");
                            }
                            else{                                
                                out.println("HTTP/1.0 200 OK");
                            }
                            // Send the response
                            // Send the headers
                            out.println("Content-Type: text/html");
                            out.println("Server: Bot");
                            out.println("Content-Length: " + correspondingFile.length());
                        }
                        
                        else {
                            while(!str.equals(""))
                                str = in.readLine();
                            
                            System.out.println("bad request");
                            out.println("HTTP/1.0 400 Bad request");
                            out.println("Content-Type: text/html");
                            out.println("Server: Bot");
                            out.println("Content-Length: 0");
                        }
                        
                        System.out.println("Done. (o^-^)o");
                        
                    }
                    
                    catch(Exception e)
                    {
                        out.println("HTTP/1.0 500 Internal server error");
                        System.out.println(e);
                    }
                }
                remote.close();
                out.flush();
            } catch (Exception e) {
                System.out.println("Error: " + e);
            }
        }
        
    }
    
    /**
     * Start the application.
     *
     * @param args
     * Command line parameters are not used.
     */
    public static void main(String args[]) {
        WebServer ws = new WebServer();
        ws.start();
    }
}
