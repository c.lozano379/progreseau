/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package UDPGUI;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author cloza
 */
public class TestProgReseau {
    
    static private JFrame frame;
    static private JPanel panel;
    static private JScrollPane chat;
    static private JLabel label;
    static private JTextField tf;
    static private JButton send;
    static private JButton reset;
    static private JTextArea ta;
    
    static private  boolean endChat = false;
    static private String pseudo;
    
    static private MulticastSocket echoSocket;
    static private InetAddress group;
    static private int port;
    
    static private ListeningThread lt;
    
    public static void initGUI(){
        frame = new JFrame("Group chat");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500,500);
        
        panel = new JPanel();
        label = new JLabel("Message : ");
        tf = new JTextField(100);
        send = new JButton("Send");
        reset = new JButton("Cancel");
        ta = new JTextArea();
        ta.setEditable(false);
        chat = new JScrollPane(ta);
        
        send.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String line=tf.getText();
                String message = "[" + pseudo + "] " + line + "\n";
                
                try{
                    DatagramPacket toSend = new DatagramPacket(message.getBytes(), message.length(), group, port);
                    echoSocket.send(toSend);
                    tf.setText("");
                }catch(Exception exc){
                    System.err.println(exc);
                }
            }
        });
        reset.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tf.setText("");
            }
        });
        
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                lt.stop();
                String message = pseudo + " left the chat (o^-^)o \n";
                
                try{
                    DatagramPacket toSend = new DatagramPacket(message.getBytes(), message.length(),
                            group, port);
                    echoSocket.send(toSend);
                    echoSocket.leaveGroup(group);
                }catch(IOException exc){
                    System.err.println("envoi pseudo : " + exc);
                }
            }
        });
        
        panel.setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.BOTH;
        
        constraints.weightx = 0.4;
        constraints.weighty = 0.05;
        constraints.gridx = 0;
        constraints.gridy = 6;
        panel.add(label, constraints);
        
        constraints.weightx = 2.0;
        constraints.weighty = 0.05;
        constraints.gridx = 1;
        constraints.gridy = 6;
        panel.add(tf, constraints);
        
        constraints.weightx = 0.5;
        constraints.weighty = 0.05;
        constraints.gridx = 2;
        constraints.gridy = 6;
        panel.add(send, constraints);
        
        constraints.weightx = 0.5;
        constraints.weighty = 0.05;
        constraints.gridx = 3;
        constraints.gridy = 6;
        panel.add(reset, constraints);
        
        constraints.weightx = 1.0;
        constraints.weighty = 1.0;
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.gridheight = 6;
        constraints.gridwidth = 4;
        panel.add(chat, constraints);
        
        frame.setContentPane(panel);
        frame.setVisible(true);
    }
    
    public static void pseudoChoice(){
        pseudo = JOptionPane.showInputDialog(frame,"Choose a name", null);
        String message = pseudo + " a rejoint le chat !\n";
        
        try{
            DatagramPacket toSend = new DatagramPacket(message.getBytes(), message.length(), group, port);
            echoSocket.send(toSend);
        }catch(Exception e){
            System.err.println("envoi pseudo : " + e);
        }
    }
    
    /**
     *  main method
     *  accepts a connection, receives a message from client then sends an echo to the client
     **/
    public static void main(String[] args) throws IOException {
        echoSocket = null;
        pseudo = null;
        group = null;
        port = 42000;
        
        if (args.length != 2) {
            System.out.println("Usage: java EchoClient <EchoServer host> <EchoServer port>");
            System.exit(1);
        }

        try {
            //creation socket ==> connexion
            port = new Integer(args[1]).intValue();
            echoSocket = new MulticastSocket(port);
            group = InetAddress.getByName(args[0]);
            echoSocket.joinGroup(group);
        }
        catch (Exception e) {
            System.err.println(e);
        }
        
        initGUI();
        pseudoChoice();

        lt = new ListeningThread(echoSocket, ta, chat);
        lt.start();

    }
    
}
