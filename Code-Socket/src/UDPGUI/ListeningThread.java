/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package UDPGUI;

import java.net.DatagramPacket;
import java.net.MulticastSocket;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 *
 * @author cloza
 */
public class ListeningThread extends Thread{
    private MulticastSocket listeningSocket;
    private JTextArea ta;
    private JScrollPane chatBox;
    
    ListeningThread(MulticastSocket s, JTextArea ta, JScrollPane chatBox){
        this.listeningSocket = s;
        this.ta = ta;
        this.chatBox = chatBox;
    }
    
    /**
     * Receive and print message in the GUI
     **/
    public void run() {
        try {
            while (true) {
                byte[] buf = new byte[1000];
                DatagramPacket recv = new DatagramPacket(buf, buf.length);
                listeningSocket.receive(recv);
                String received = new String(recv.getData(), 0, recv.getLength());
                ta.append(received);
                JScrollBar vertical = chatBox.getVerticalScrollBar();
                vertical.setValue( vertical.getMaximum() );
            }
        }
        catch (Exception e) {
            System.err.println("Error in EchoServer:" + e);
        }
        
    }
}
