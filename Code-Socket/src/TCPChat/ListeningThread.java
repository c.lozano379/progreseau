/***
 * ClientThread
 * Example of a TCP server
 * Date: 14/12/08
 * Authors:
 */

package TCPChat;

import java.io.*;
import java.net.*;

public class ListeningThread
	extends Thread {
	
	private Socket listeningSocket;
	
	ListeningThread(Socket s) {
		this.listeningSocket = s;
	}

 	/**
  	* listen to the server and print the received messages to the terminal
  	**/
	public void run() {
    	  try {
    		BufferedReader socIn = null;
    		socIn = new BufferedReader(new InputStreamReader(listeningSocket.getInputStream()));   
    		while (true) {
                    String line = socIn.readLine();
                    if(line != null)
                    System.out.println(line);
    		}
    	} catch (Exception e) {
        	System.err.println("Error in EchoServer:" + e); 
        }
       }
  
  }

  
