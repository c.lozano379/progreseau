/***
 * ClientThread
 * Example of a TCP server
 * Date: 14/12/08
 * Authors:
 */

package TCPChat;

import java.io.*;
import java.net.*;

public class ClientThread extends Thread {
	
	private Socket clientSocket;
	
	ClientThread(Socket s) {
		this.clientSocket = s;
	}

 	/**
  	* receives a request from client then sends an echo to the client
  	**/
  	
	public void run() {
            try {
                BufferedReader socIn = null;
                socIn = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));  
                boolean clientDisconnecting = false;
    		while (true) {
                    String line = socIn.readLine();
                    String tmp = line.substring(0,5);
                    if(tmp.equals("/exit"))
                    {
						System.out.println("EXITING");
						line = line.substring(6) + " has disconnected from chat server.";
						clientDisconnecting = true;
					}
                    System.out.println("Posted : " + line);
                    for(int i = 0; i < EchoServerMultiThreaded.connectedClients.size(); i++)
                    {  
                        if(EchoServerMultiThreaded.connectedClients.get(i) != clientSocket && !clientDisconnecting)
                        {
                            PrintStream socOut = new PrintStream(EchoServerMultiThreaded.connectedClients.get(i).getOutputStream());
                            socOut.println(line);
                        }
                        else if(clientDisconnecting)
                        {
							EchoServerMultiThreaded.connectedClients.get(i).close();
							EchoServerMultiThreaded.connectedClients.remove(i);
							i--;
						}
                    }
                    if(clientDisconnecting)
						return;
                }
            } catch (Exception e) {
                System.err.println("Error in EchoServer:" + e); 
            }
        }
  
  }

  
