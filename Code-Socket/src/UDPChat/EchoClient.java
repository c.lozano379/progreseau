/***
 * EchoClient
 * Example of a TCP client 
 * Date: 10/01/04
 * Authors:
 */
package UDPChat;

import java.io.*;
import java.net.*;
import java.util.ArrayList;



public class EchoClient{

	
	public static ArrayList<String> history;

	/**
	*  main method
	*  accepts a connection, receives a message from client then sends an echo to the client
	**/
	public static void main(String[] args) throws IOException {

		history = new ArrayList<String>();

		MulticastSocket echoSocket = null;
		String pseudo = null;
		InetAddress group = null;
		int port = 9999;

		//parsing args
		if (args.length != 2) {
			System.out.println("Usage: java EchoClient <EchoServer host> <EchoServer port>");
			System.exit(1);
		}

		try {
			// creation socket ==> connexion
			port = new Integer(args[1]).intValue();
			echoSocket = new MulticastSocket(port);
			group = InetAddress.getByName(args[0]);
			echoSocket.joinGroup(group);
		} 
		catch (Exception e) {
			System.err.println(e);
		}

		//defining pseudo
		System.out.println("Choisissez un pseudo : ");
		BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
		pseudo = stdIn.readLine();
		String message = "/join " + pseudo + " a rejoint le chat !";
		DatagramPacket toSend = new DatagramPacket(message.getBytes(), message.length(),
		group, port);
		echoSocket.send(toSend);
		
		//starting listening thread
		ListeningThread lt = new ListeningThread(echoSocket, group, port, pseudo);
		lt.start();             

		//handling input
		String line;
		while (true) {
			line=stdIn.readLine();
			
			//handling /exit (logging)
			if (line.equals("/exit")) 
			{				
				File newFile = new File("UDPChatLogs/log_"+pseudo+".txt");
				boolean fileExisted = !(newFile.createNewFile());
				
				BufferedWriter logWriter = new BufferedWriter(new FileWriter("UDPChatLogs/log_"+pseudo+".txt",true));
				
				if(fileExisted)
					logWriter.write("");
				
				for(int i = 0; i < EchoClient.history.size(); i++)
				{
					logWriter.append(EchoClient.history.get(i) + "\n");
				}
				logWriter.close();
				break;
			}
			
			//sending message
			message = "[" + pseudo + "] " + line;
			toSend = new DatagramPacket(message.getBytes(), message.length(),
			group, port);
			echoSocket.send(toSend);
		}
		
		lt.stop();
		message = pseudo + " left the chat (o°-°)o            ";
		toSend = new DatagramPacket(message.getBytes(), message.length(),
		group, port);
		echoSocket.send(toSend);
		echoSocket.leaveGroup(group);
	}
}




