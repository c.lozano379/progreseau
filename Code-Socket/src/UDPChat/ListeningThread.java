/***
* ClientThread
* Example of a TCP server
* Date: 14/12/08
* Authors:
*/

package UDPChat;

import java.io.*;
import java.net.*;

public class ListeningThread
extends Thread {

	private MulticastSocket listeningSocket;
	private InetAddress group;
	private int port;
	private String pseudo;

	ListeningThread(MulticastSocket s, InetAddress group, int port, String pseudo) {
		this.listeningSocket = s;
		this.group = group;
		this.port = port;
		this.pseudo = pseudo;
	}

	/**
	* receives a request from client then sends an echo to the client
	* @param listeningSocket the client socket
	**/
	public void run() {
		
		boolean overpassReceive = false;
		String received;
		
		try {
			String historySender = null;
			
			System.out.println("------------------History-----------------");
			//receiving history
			while(true)
			{
				byte[] buf = new byte[1000];
				DatagramPacket recv = new DatagramPacket(buf, buf.length);
				listeningSocket.receive(recv);
				received = new String(recv.getData(), 0, recv.getLength());
				
				//starting history receiving
				if(received.length() > 14 && received.substring(0,14).equals("/starthistory_"))
				{
					if(historySender == null)
					{
						historySender = received.substring(14);
					}
				}
				
				//receiving history
	            if(historySender != null && received.length() > 9 && received.substring(0, 9 + historySender.length()).equals("/history_" +historySender))
	            {
					EchoClient.history.add(received.substring(9 + historySender.length()));
					System.out.println(EchoClient.history.get(EchoClient.history.size()-1));
				}
				
				//ending receiving
				if(historySender != null && received.length() > 12 && received.substring(0, 12 + historySender.length()).equals("/endhistory_" + historySender))
					break;
					
				//aborting receiving on other's join
				if(received.length() > 5 && received.substring(0, 5).equals("/join") && !received.substring(6, 6+pseudo.length()).equals(pseudo))
				{
					overpassReceive = true;
					break;
				}
				
				//on receiving own's join
				if(received.length() > 5 && received.substring(0, 5).equals("/join") && received.substring(6, 6+pseudo.length()).equals(pseudo))
				{
					received = received.substring(6);
					EchoClient.history.add(received);
				}
			}
			
			System.out.println("------------------------------------------");
			
			//receiving new messages
			while (true) {
				
				//receiving incoming message
				if(!overpassReceive)
				{
				byte[] buf = new byte[1000];
					DatagramPacket recv = new DatagramPacket(buf, buf.length);
					listeningSocket.receive(recv);
					received = new String(recv.getData(), 0, recv.getLength());
				}
				else
					overpassReceive = false;
				
				//on other's /join	
				if(received.length() > 5 && received.substring(0,5).equals("/join") && !received.substring(6, 6+pseudo.length()).equals(pseudo))
				{
					received = received.substring(6);
					
					//send history
					String message = "/starthistory_" + pseudo;
					DatagramPacket toSend = new DatagramPacket(message.getBytes(), message.length(), group, port);
					this.listeningSocket.send(toSend);
					
					for(int i = 0; i < EchoClient.history.size(); i++)
					{
						message = "/history_" + pseudo + " " + EchoClient.history.get(i);
						toSend = new DatagramPacket(message.getBytes(), message.length(), group, port);
						this.listeningSocket.send(toSend);
					}
					
					message = "/endhistory_" + pseudo;
					toSend = new DatagramPacket(message.getBytes(), message.length(), group, port);
					this.listeningSocket.send(toSend);
				}
				
				//ignoring own messages
				if(received.length() >= 2 + pseudo.length() && received.substring(0, 2 + pseudo.length()).equals("[" + pseudo + "]"))
				{
					EchoClient.history.add(received);
					continue;
				}
				
				//ignoring history messages
				if(received.length() > 9 && received.substring(0,9).equals("/history_")
				|| received.length() > 12 && received.substring(0,12).equals("/endhistory_")
				|| received.length() > 14 && received.substring(0,14).equals("/starthistory_")
				)
					continue;
					
				EchoClient.history.add(received);
	            System.out.println(received);
			}
		}
		catch (Exception e) {
			System.err.println("Error in EchoServer:" + e); 
		}

	}
}

  
